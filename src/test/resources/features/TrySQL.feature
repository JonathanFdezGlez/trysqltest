Feature: Try SQL
	As a W3C user
  I want to have a Try SQL funcionality
  So that I can test SQL online 
  
	Scenario: Check Elements
		Then the top ad is displayed
		And "SQL Statement:" text element is displayed
		And "Result:" text element is displayed
		And Your database table is displayed
		And Try SQL Editor area is displayed

  Scenario: Create table
    Given I write SQL to create a table "Test"
    When I click on Run SQL button
    Then I can check the table "Test" has been created
    And has no records on it

  Scenario: Check default text in result is displayed
   	Then The text "Run SQL" is displayed in result area
   	
  Scenario: Check customers from Spain
		Given I write SQL to find customers from "Spain"
		When I click on Run SQL button
		Then I expect to see 3 customers from Spain   

	Scenario: Check Notes for employee given dates 
		Given I write SQL to find employees born between "1925-01-01" and "1930-12-31"
		When I click on Run SQL button
		Then I expect the notes is "An old chum."  
		
	Scenario: Check product ordered in the largest quantity
		Given I write SQL to get the product ordered with largest quantity
		When I click on Run SQL button
		Then I expect the product name to be "Longlife Tofu" 
		And I expect the quantity of the order to be 100
		
	Scenario: Check employee who ordered on the 30th Jan 1997
		Given I write SQL to get orders dated on "1997-01-30"
		When I click on Run SQL button
		Then I expect the employeeId 4 to be the one who ordered
package PageObjects;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TrySQLObject{	
	
	private WebDriver driver;
	private WebDriverWait wait;
	
	public TrySQLObject(WebDriver driver){
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 10);
	}
	
	public Boolean CheckAdIsDisplayed() {
		String adId = "#tryitLeaderboard iframe";
		WebElement adElement =  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(adId)));
		return adElement.isDisplayed();
	}
	
	public Boolean CheckElementWithTextIsDisplayed(String textElement) {
		String elementWithTextSelector = "//*[text() = '" + textElement + "']";
		WebElement elementWithText =  driver.findElement(By.xpath(elementWithTextSelector));
		return elementWithText.isDisplayed();
	}
	
	public Boolean CheckMyDBIsDisplayed() {
		String myDBId = "yourDB";
		WebElement myDBElement = driver.findElement(By.id(myDBId));
		return myDBElement.isDisplayed();
	}
	
	public Boolean CheckTrySQLEditorIsDisplayed() {
		String trySQLSelector = ".w3-col > h3";
		WebElement trySQLElement = driver.findElement(By.cssSelector(trySQLSelector));
		return trySQLElement.isDisplayed();
	}
	
	public void TypeQueryInTextArea(String query) {
		String queryAreaSelector = ".CodeMirror";
		WebElement queryInput = driver.findElements(By.cssSelector(queryAreaSelector)).get(0);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].CodeMirror.setValue(\"" + query + "\");", queryInput);
	}
	
	public void ClickRunSQL() {
		String selector = "//button[text() = 'Run SQL �']";
		driver.findElement(By.xpath(selector)).click();;
	}

	public Boolean CheckTableExistsByName(String tableName) {
		String myTableCellSelector = "//td[text() = '" + tableName + "']";
		WebElement myTable =  wait.until(ExpectedConditions.elementToBeClickable(By.xpath(myTableCellSelector)));
		return myTable.isDisplayed();
	}
	
	public int CheckRecordsOnTable() {
		// Given I need the text of my DB and I can't get the parent through it to access to the last td with the records, 
		// I've noticed the table will be added at the end of the table DB, so this will work regardless the DB name
		String myRecordsSelector = "#yourDB table tr:last-child td:last-child";
		WebElement records = driver.findElement(By.cssSelector(myRecordsSelector));
		return Integer.parseInt(records.getText());
	}
	
	public Boolean CheckRunSQLTextIsDisplayed(String textElement) {
		String resultAreaSelector = "#divResultSQL div";
		WebElement resultAreaElement =  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(resultAreaSelector)));
		return resultAreaElement.getText().contains(textElement);
	}

	public int GetNumberOfEntriesInDB() {
		String countDBResultSelector = "#divResultSQL td";
		WebElement resultDB = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(countDBResultSelector)));
		return Integer.parseInt(resultDB.getText());
	}
	
	public String GetSQLResultByColumnIndex(int columnIndex) {
		String resultAreaSelector = "#divResultSQL td:nth-child(" + columnIndex + ")";
		WebElement resultAreaElement =  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(resultAreaSelector)));
		return resultAreaElement.getText();
	}

	public ArrayList<Integer> GetSQLResultListEmployee() {
		String getListResultSelector = "#divResultSQL td";
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(getListResultSelector)));
		java.util.List<WebElement> listResults = driver.findElements(By.cssSelector(getListResultSelector));
		ArrayList<Integer> employeeList = new ArrayList<Integer>();
		for(int i = 0; i < listResults.size(); i++) {
			employeeList.add(Integer.parseInt(listResults.get(i).getText()));
		}
		return employeeList;
	}
	
}

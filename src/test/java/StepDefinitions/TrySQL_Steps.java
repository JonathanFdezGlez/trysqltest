package StepDefinitions;

import java.util.ArrayList;

import org.testng.Assert;

import PageObjects.TrySQLObject;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TrySQL_Steps extends TestBase{
	
	private TrySQLObject trySQLObject = new TrySQLObject(driver);
	
	@Before
    public void InitializeTest(){
       String baseURL = "https://www.w3schools.com/sql/trysql.asp?filename=trysql_select_distinct";
       driver.get(baseURL);
    }
 
    @After
    public void TearDown(Scenario scenario) {
        driver.quit();
    }
	
	@Then("^the top ad is displayed$")
	public void the_top_ad_is_displayed(){
	    Boolean currentState = trySQLObject.CheckAdIsDisplayed();
	    Assert.assertTrue(currentState, "The ad was not displayed");
	}

	@Then("^\"([^\"]*)\" text element is displayed$")
	public void text_element_is_displayed(String textElement){
		Boolean currentState = trySQLObject.CheckElementWithTextIsDisplayed(textElement);
	    Assert.assertTrue(currentState, "The element was not displayed");
	}

	@Then("^Your database table is displayed$")
	public void your_database_table_is_displayed(){
		Boolean currentState = trySQLObject.CheckMyDBIsDisplayed();
	    Assert.assertTrue(currentState, "My DB was not displayed");
	}

	@Then("^Try SQL Editor area is displayed$")
	public void try_SQL_Editor_area_is_displayed(){
		Boolean currentState = trySQLObject.CheckTrySQLEditorIsDisplayed();
	    Assert.assertTrue(currentState, "Try SQL Editor was not displayed");
	}
		
	@Given("^I write SQL to create a table \"([^\"]*)\"$")
	public void i_write_SQL_to_create_a_table(String tableName) {
	    String query = "CREATE TABLE " + tableName + " (ID int);";
	    trySQLObject.TypeQueryInTextArea(query);
	}

	@When("^I click on Run SQL button$")
	public void i_click_on_Run_SQL_button(){
		trySQLObject.ClickRunSQL();
	}

	@Then("^I can check the table \"([^\"]*)\" has been created$")
	public void i_can_check_the_table_has_been_created(String tableName) {
	    Boolean currentResult = trySQLObject.CheckTableExistsByName(tableName);
	    Assert.assertTrue(currentResult, "The table hasn't been created");
	}

	@Then("^has no records on it$")
	public void has_no_records_on_it(){
		int numberOfRecords = trySQLObject.CheckRecordsOnTable();
		Assert.assertEquals(numberOfRecords, 0, "The number of records isn't 0");
	}
	
	@Then("^The text \"([^\"]*)\" is displayed in result area$")
	public void the_text_is_displayed_in_result_area(String textElement) {
		Boolean currentState = trySQLObject.CheckRunSQLTextIsDisplayed(textElement);
	    Assert.assertTrue(currentState, "The element result area does not contain the text " + textElement);
	}
	
	@Given("^I write SQL to find customers from \"([^\"]*)\"$")
	public void i_write_SQL_to_find_customers_from(String country) {
	    String query = "SELECT COUNT(*) 'Number of Customers' FROM Customers WHERE Country = '" + country + "';";
	    trySQLObject.TypeQueryInTextArea(query);
	}
	
	@Then("^I expect to see (\\d+) customers from Spain$")
	public void i_expect_to_see_customers_from_Spain(int expectedCustomersNumber) {
	    int numberOfEntries = trySQLObject.GetNumberOfEntriesInDB();
	    Assert.assertEquals(numberOfEntries, expectedCustomersNumber);
	}
	
	@Given("^I write SQL to find employees born between \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_write_SQL_to_find_employees_born_between_and(String date1, String date2) {
	    String query = "SELECT Notes FROM Employees WHERE BirthDate BETWEEN '" + date1 + "' AND '" + date2 + "';";
		trySQLObject.TypeQueryInTextArea(query);
	}

	@Then("^I expect the notes is \"([^\"]*)\"$")
	public void i_expect_the_notes_is(String notes) {
	    String currentNotes = trySQLObject.GetSQLResultByColumnIndex(1);
	    Assert.assertEquals(currentNotes, notes, "The notes were not matching");
	}
	
	@Given("^I write SQL to get the product ordered with largest quantity$")
	public void i_write_SQL_to_get_the_product_ordered_with_largest_quantity() {
	    String query = "SELECT P.ProductName, O.Quantity FROM Products P JOIN OrderDetails O ON P.ProductId = O.ProductId ORDER BY Quantity DESC LIMIT 1;";
	    trySQLObject.TypeQueryInTextArea(query);
	}

	@Then("^I expect the product name to be \"([^\"]*)\"$")
	public void i_expect_the_product_name_to_be(String productName) {
		String currentProductName = trySQLObject.GetSQLResultByColumnIndex(1);
	    Assert.assertEquals(currentProductName, productName, "The expected product name and the actual one were not matching");
	}
	
	@Then("^I expect the quantity of the order to be (\\d+)$")
	public void i_expect_the_quantity_of_the_order_to_be(int expectedQuantity) {
		String currentQuantity = trySQLObject.GetSQLResultByColumnIndex(2);
	    Assert.assertEquals(Integer.parseInt(currentQuantity), expectedQuantity, "The expected quantity and the actual one were not matching");
	}
	
	@Given("^I write SQL to get orders dated on \"([^\"]*)\"$")
	public void i_write_SQL_to_get_orders_dated_on(String date) {
		String query = "SELECT EmployeeId FROM Orders WHERE OrderDate = '" + date + "';";
	    trySQLObject.TypeQueryInTextArea(query);
	}

	@Then("^I expect the employeeId (\\d+) to be the one who ordered$")
	public void i_expect_the_employeeId_to_be_the_one_who_ordered(int employeeId) {
		ArrayList<Integer> employeesList = trySQLObject.GetSQLResultListEmployee();
		for(int i = 0; i < employeesList.size(); i++) {
			Assert.assertEquals(employeesList.get(i).intValue(), employeeId, "The expected employee was " + employeeId + ", but was " + employeesList.get(i));
		}
	}

}

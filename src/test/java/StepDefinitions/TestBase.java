package StepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TestBase {
	
	protected WebDriver driver; 
	protected  TestBase() {
		ChromeOptions options = new ChromeOptions();
	    options.addArguments("--start-maximized");
	    driver = new ChromeDriver(options);
	}

}
